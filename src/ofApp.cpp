#include "ofApp.h"
#include <iostream>
#include <string>
using namespace std;

//--------------------------------------------------------------
void ofApp::setup(){
    ofHideCursor();
    current_spec = 0;
    spec_sentence = "Loading sentence...";
    spec_age = "Loading age...";
    spec_accent = "Loading accent...";
    
    //load in json
    ofFile file("cleaned_test_dataframe_1484.json");
    
    if (file.exists()){
        file >> js;
        cout << js.size() << endl;
    }
    
    //setupGui();
    
    cam.enableMouseInput();
    cam.setDistance(-10000000000.0);
    
    sphere.setRadius(200);
    sphere.setResolution(100);

    ofLoadImage(texture,"specs/common_voice_en_1075.mp3.png");

    float w = texture.getWidth();
    float h = texture.getHeight();
    sphere.mapTexCoords(0, h, w, 0);
    sphere.rotate(180, 0, 1, 0);
    
     vector<glm::vec<3, float, glm::packed_highp>>& vertices = sphere.getMesh().getVertices();
    
     ofPixels pixels;
     texture.readToPixels(pixels);
     for (int i=0; i<vertices.size(); i++) {
         ofVec2f t = sphere.getMesh().getTexCoords()[i];
         t.x = ofClamp( t.x, 0, pixels.getWidth()-1 );
         t.y = ofClamp( t.y, 0, pixels.getHeight()-1 );
         float br = pixels.getColor(t.x, t.y).getBrightness();
         vertices[i] *= 1 + br / 255.0 ; // * some_extrude_factor
     }
    
    image.load("specs/common_voice_en_1075.mp3.png");
    spin = 0;
    
    font.load("fonts/futura/FuturaPTBook.otf", 24);
    font.setLetterSpacing(1.5);
    
    videoPlayer.load("dm_alex_metaball_test.mp4");
    videoPlayer.setLoopState(OF_LOOP_NORMAL);
    videoPlayer.play();
    videoPlayer.setVolume(0.0);
    showMetaBallVideo = false;
    lastTime = ofGetElapsedTimef();
    
    minTimeUntilNextPlay = 300.0;
    maxTimeUntilNextPlay = 600.0;
    
    minPlayingTime = 60.0;
    maxPlayingTime = 120.0;
    timeUntilNextPlay = ofRandom(minTimeUntilNextPlay, maxTimeUntilNextPlay); // hard coded is fine as well
    playingTime = ofRandom(minTimeUntilNextPlay, maxTimeUntilNextPlay); // hard coded is fine as well
}

//--------------------------------------------------------------
void ofApp::update() {
    if (!player.isPlaying()){
        cout << "player is not playing, going to next track" << endl;
        goToNextTrack();
    }
    
    sphere.rotate(-0.25, 0, 1, 0);
    spin++;
    if (spin > 360){
        spin = 0;
    }
    videoPlayer.update();
    float currentTime = ofGetElapsedTimef();
    if (currentTime - lastTime > timeUntilNextPlay){ // play in 5 seconds
        showMetaBallVideo = true;
        videoPlayer.setVolume(1.0);
        player.setVolume(0.0);
        lastTime = currentTime;
        timeUntilNextPlay = ofRandom(minTimeUntilNextPlay, maxTimeUntilNextPlay);
        playingTime = ofRandom(minTimeUntilNextPlay, maxTimeUntilNextPlay);
    }
    
    if ((currentTime - lastTime) > playingTime && showMetaBallVideo == true){
        showMetaBallVideo = false;
        videoPlayer.setVolume(0.0);
        player.setVolume(1.0);
        lastTime = currentTime;
        timeUntilNextPlay = ofRandom(minTimeUntilNextPlay, maxTimeUntilNextPlay);
        playingTime = ofRandom(minTimeUntilNextPlay, maxTimeUntilNextPlay);
    }
}

//--------------------------------------------------------------
void ofApp::draw() {


    ofBackground(22, 22, 29);
    
    cam.begin();
    draw3d();
    cam.end();
    
    /*
    ofDrawBitmapString(spec_sentence, 10, 10);
    ofDrawBitmapString(spec_age, 10, 30);
    ofDrawBitmapString(spec_accent, 10, 50);
    */
    
    //drawGui();
    
    //image.draw(20 , gui.getHeight() + 40, image.getWidth()/3, image.getHeight()/3);
    
    // long spec sentence
    //spec_sentence = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vestibulum ipsum sit amet dui efficitur, ac rhoncus turpis placerat. Mauris quis scelerisque odio. Etiam erat lorem, viverra in laoreet sed. ";
    // check length of spec_sentence
    if (font.stringWidth(spec_sentence) > ofGetScreenWidth()){
        cout << "long string" << endl;
        // break string in half
        int offset;
        int len = spec_sentence.length();
        if (&spec_sentence[len/2] != " "){
            offset = 1;
        }
        
        string first_half = spec_sentence.substr(0, (len/2) + offset);
        
        string second_half = spec_sentence.substr((len/2) + offset);

        font.drawString(first_half, ofGetScreenWidth()/2 - (font.stringWidth(first_half) / 2), ofGetScreenHeight() - 80);
        font.drawString(second_half, ofGetScreenWidth()/2 - (font.stringWidth(second_half) / 2), ofGetScreenHeight() - 30);
        
    } else{
        font.drawString(spec_sentence, ofGetScreenWidth()/2 - (font.stringWidth(spec_sentence) / 2), ofGetScreenHeight() - 50);
    }
    
    font.drawString(spec_age, 50, 50);
    font.drawString(spec_accent, 50, 100);
    char current_spec_string[255];
    sprintf(current_spec_string, "%i / %i", current_spec, js.size());
    font.drawString(current_spec_string , 50, 150);
    
    if (showMetaBallVideo){
        videoPlayer.draw(0,0);
    }
    
}

void ofApp::draw3d(){
    ofScale(1, -1, 1);
    texture.bind();
    //light.setPosition(ofGetWidth() / 2, ofGetHeight()/2, 600);
    //light.enable();
    //material.begin();
    ofEnableDepthTest();
    
    ofSetColor(ofColor::white);
    sphere.draw();
    sphere.drawWireframe();
 
    ofDisableDepthTest();
    //material.end();
    //light.disable();
    //ofDisableLighting();
    texture.unbind();

}

void ofApp::setupGui(){
        gui.setup("DROPDOWNS", "DropdownsSettings.xml");
        gui.setPosition(20,20);
        
        strDropdown =  make_unique<ofxDropdown>("");

        strDropdown->addListener(this, &ofApp::togglePressed);
        options.setName("STR Options");

        ofSetWindowPosition(0, 0);
    
        //get specs
        //some path, may be absolute or relative to bin/data
        string path = "specs";
    
        ofDirectory dir(path);
        //only show png files
        dir.allowExt("png");
        //populate the directory object
        dir.listDir();

    //go through and print out all the paths
    for(int i = 0; i < dir.size(); i++){
        //ofLogNotice(dir.getPath(i));
        strDropdown->add(dir.getPath(i));
        }
    
        gui.add(strDropdown.get());

}

void ofApp::drawGui(){
        gui.draw();
    
}

void ofApp::togglePressed(string & newValue){
    cout << newValue << endl;

    image.load(newValue);
    
    sphere = ofSpherePrimitive();
    sphere.setRadius(200);
    sphere.setResolution(100);
    
    ofLoadImage(texture, newValue);
    
    float w = texture.getWidth();
    float h = texture.getHeight();
    sphere.mapTexCoords(0, h, w, 0);
    sphere.rotate(180, 0, 1, 0);
    
    vector<glm::vec<3, float, glm::packed_highp>>& vertices = sphere.getMesh().getVertices();
    
    ofPixels pixels;
    texture.readToPixels(pixels);
    for (int i=0; i<vertices.size(); i++) {
        ofVec2f t = sphere.getMesh().getTexCoords()[i];
        t.x = ofClamp( t.x, 0, pixels.getWidth()-1 );
        t.y = ofClamp( t.y, 0, pixels.getHeight()-1 );
        float br = pixels.getColor(t.x, t.y).getBrightness();
        vertices[i] *= 1 + br / 255.0 ; // * some_extrude_factor
    }
    
    
    // save .ply file
    string ply_file_name = newValue;
    string ply("ply");
    ply_file_name.replace(ply_file_name.find("png"),ply.length(),"ply");
    cout << ply_file_name << endl;
    sphere.getMesh().save(ply_file_name);
    
    // play audio
    string wav_file_name = newValue;
    string wav("wav");
    wav_file_name.replace(wav_file_name.find("png"),wav.length(),"wav");
    //cout << wav_file_name << endl;
    
    // play audio - mp3
    string mp3_file_name = newValue;
    string png(".png");
    mp3_file_name.replace(mp3_file_name.find(".png"),png.length(),"");
    //cout << mp3_file_name << endl;
    
    
    player.load(mp3_file_name);
    player.setLoop(false);
    player.play();
    

}
//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if(key >= '0' && key <= '9'){
        intOptions = key - '0';
    }else if(key == 's'){
        gui.saveToFile("DropdownsSettings.xml");
    }else if(key == 'l'){
        gui.loadFromFile("DropdownsSettings.xml");
    }else if(key == ' '){
        //get specs
        
        /*
        //some path, may be absolute or relative to bin/data
        string path = "specs";
    
        ofDirectory dir(path);
        //only show png files
        dir.allowExt("png");
        //populate the directory object
        dir.listDir();

        cout << current_spec << endl;
        
        
        string spec_path = dir.getPath(current_spec);
        */
        
        auto & spec = js[current_spec];
        
        string spec_png_path = "specs/" + string(spec["path"]) + ".png" ;
        string spec_mp3_path = "specs/" + string(spec["path"]);
        cout << spec_png_path << endl;
        cout << spec_mp3_path << endl;
        
        //update bitmap strings
        spec_sentence = spec["sentence"];
        spec_age = spec["age"];
        spec_accent = spec["accent"];
        
        image.load(spec_png_path);
        
        sphere = ofSpherePrimitive();
        sphere.setRadius(200);
        sphere.setResolution(100);
        
        ofLoadImage(texture, spec_png_path);
        
        float w = texture.getWidth();
        float h = texture.getHeight();
        sphere.mapTexCoords(0, h, w, 0);
        sphere.rotate(180, 0, 1, 0);
        
        vector<glm::vec<3, float, glm::packed_highp>>& vertices = sphere.getMesh().getVertices();
        
        ofPixels pixels;
        texture.readToPixels(pixels);
        for (int i=0; i<vertices.size(); i++) {
            ofVec2f t = sphere.getMesh().getTexCoords()[i];
            t.x = ofClamp( t.x, 0, pixels.getWidth()-1 );
            t.y = ofClamp( t.y, 0, pixels.getHeight()-1 );
            float br = pixels.getColor(t.x, t.y).getBrightness();
            vertices[i] *= 1 + br / 255.0 ; // * some_extrude_factor
        }
        
        player.load(spec_mp3_path);
        player.setLoop(false);
        player.play();
        
        // go to next spec
        current_spec += 1;
    }
}

void ofApp::goToNextTrack(){
    auto & spec = js[current_spec];
    
    string spec_png_path = "specs/" + string(spec["path"]) + ".png" ;
    string spec_mp3_path = "specs/" + string(spec["path"]);
    cout << spec_png_path << endl;
    cout << spec_mp3_path << endl;
    
    //update bitmap strings
    spec_sentence = spec["sentence"];
    spec_age = spec["age"];
    spec_accent = spec["accent"];
    
    image.load(spec_png_path);
    
    sphere = ofSpherePrimitive();
    sphere.setRadius(200);
    sphere.setResolution(100);
    
    ofLoadImage(texture, spec_png_path);
    
    float w = texture.getWidth();
    float h = texture.getHeight();
    sphere.mapTexCoords(0, h, w, 0);
    sphere.rotate(180, 0, 1, 0);
    
    vector<glm::vec<3, float, glm::packed_highp>>& vertices = sphere.getMesh().getVertices();
    
    ofPixels pixels;
    texture.readToPixels(pixels);
    for (int i=0; i<vertices.size(); i++) {
        ofVec2f t = sphere.getMesh().getTexCoords()[i];
        t.x = ofClamp( t.x, 0, pixels.getWidth()-1 );
        t.y = ofClamp( t.y, 0, pixels.getHeight()-1 );
        float br = pixels.getColor(t.x, t.y).getBrightness();
        vertices[i] *= 1 + br / 255.0 ; // * some_extrude_factor
    }
    
    player.load(spec_mp3_path);
    player.setLoop(false);
    player.play();
    
    // go to next spec
    current_spec += 1;

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){


}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
