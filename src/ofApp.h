#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxDropdown.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();
        
        void keyPressed  (int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
        
        // drawing sphere
        void draw3d();
        ofTexture texture;
        ofMesh mesh;
        ofEasyCam cam;
        ofSpherePrimitive sphere;
        ofLight light;
        ofMaterial material;
        ofFbo fbo;
    
        // gui
        ofxPanel gui;
        //ofxGuiGroup gui;
        ofParameter<string> options;//, options2;
        ofParameter<int> intOptions;
        unique_ptr<ofxDropdown> strDropdown;
        void setupGui();
        void drawGui();
        void togglePressed(string & newValue);
    
        ofSoundPlayer player;
        ofImage image;
    
        int current_spec;
        string spec_sentence;
        string spec_age;
        string spec_accent;
    
        ofJson js;
    
        void goToNextTrack();
    
        int spin;
    
        // font
        ofTrueTypeFont font;
    
        //metaball video
        ofVideoPlayer videoPlayer;
        bool showMetaBallVideo;
        float lastTime;
        float timeUntilNextPlay;
        float playingTime;
        float minTimeUntilNextPlay;
        float maxTimeUntilNextPlay;
    
        float minPlayingTime;
        float maxPlayingTime;
};
